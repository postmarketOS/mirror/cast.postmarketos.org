# Copyright 2023 Oliver Smith
# SPDX-License-Identifier: AGPL-3.0-or-later
import html
import os
import re
import yaml


def url_templates(html):
    """ Apply various URL templates, to save time while writing them in
        markdown. Preferably they are the same as in the wiki:
        https://wiki.postmarketos.org/wiki/PostmarketOS:Templates """
    patterns = {
        # {{MR|1234|pmaports}}
        r'\{\{MR\|([0-9]+)\|([^|]+)\}\}':
            r'<a href="https://gitlab.com/postmarketOS/\2/-/merge_requests/\1">!\1</a>',
        # {{issue|1234|pmaports}}
        r'\{\{issue\|([0-9]+)\|([^|]+)\}\}':
            r'<a href="https://gitlab.com/postmarketOS/\2/-/issues/\1">#\1</a>',
        # {{patches|1234|pmbootstrap-devel}}
        r'\{\{patches\|([0-9]+)\|([^|]+)\}\}':
            r'<a href="https://lists.sr.ht/~postmarketos/\2/patches/\1">patches/\1</a>',
    }

    ret = html
    for pattern, replacement in patterns.items():
        ret = re.sub(pattern, replacement, ret)

    return ret


def replace(html):
    """ Various replacements for markdown files.
        :param html: markdown code, already converted to HTML
        :returns: html with replacements made """
    ret = html
    ret = url_templates(ret)

    return ret


def parse_yaml_md(path):
    """ Load one of the blog post / edge post / page .md files.
        :returns: data, content
                  * data: the parsed yaml frontmatter
                  * content: markdown code below "---"
    """
    with open(os.path.join(path), encoding="utf-8") as handle:
        raw = handle.read()
    frontmatter, content = config.regex_split_frontmatter.split(raw, 2)

    data = yaml.load(frontmatter, Loader=yaml.Loader)
    return data, content
