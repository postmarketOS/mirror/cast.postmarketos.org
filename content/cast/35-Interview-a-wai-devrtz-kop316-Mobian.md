date: 2023-10-25
title: "#35 INTERVIEW: a-wai, devrtz, kop316 (of Mobian Fame)"
length: "29:18"
timestamps:
  - "01:15 mmsd-tng, vvmd"
  - "02:34 DMs & DDs"
  - "10:35 FOSDEM 2024"
  - "16:00 Linux Mobile World Tour"
  - "19:42 Qualcomms and regions"
  - "21:00 Questions for us"
  - "22:54 Shout out"
  - "23:58 Best way to contribute to Mobian"
  - "25:25 Wrapping up"
---
In a somewhat chaotic and much longer recording session than what ended up in
the final episode, we had a very good time talking to our friends from Mobian.
As you can guess from the name, Mobian brings Mobile and Debian together,
similar to what postmarketOS does on top of Alpine Linux.

Featuring @kop316, @ollieparanoid, @devrtz, @calebccff, @PureTryOut, @a-wai (in
order of appearance).

[Mobian accepts donations via Liberapay.](https://liberapay.com/mobian)

Referenced in this episode:

* [Mobian](https://mobian-project.org/): A Debian derivative for mobile devices.
* [DebianMaintainer - Debian Wiki](https://wiki.debian.org/DebianMaintainer)
* [DebianDeveloper - Debian Wiki](https://wiki.debian.org/DebianDeveloper)
* [FOSDEM 2024](https://fosdem.org/2024/)
* [King Gizzard & The Lizard Wizard](https://kinggizzardandthelizardwizard.com/)

Related episodes:

* \#11 INTERVIEW: kop316 (of mmsd-tng Fame)
* \#28 FOSDEM 2023 special

<small>Editing by: @ollieparanoid,
Music by: [The Passion HiFi](http://www.thepassionhifi.com)</small>
