date: 2022-02-08
title: "#14 FOSDEM 2022 Special"
length: "18:27"
timestamps:
  - "00:10 Anyway..."
---
Shortly after @Danct12 asked about everyone's favorite singer in the extended
Q&A part of the Closing session (not recorded), a few of us met <del>outside of
FOSDEM</del> in front of our computers again to talk a bit about our experience
at FOSDEM 2022. Featuring  @ollieparanoid, @MartijnBraam, @calebccff
(in order of appearance).

Some of the videos are already up!

Referenced in this episode:

* [FOSS on Mobile Devices devroom](https://fosdem.org/2022/schedule/track/foss_on_mobile_devices/)
* [Plasma Mobile in 2022](https://fosdem.org/2022/schedule/event/kde_plasme_mobile/)
* [Bringing RAUC A/B Updates to More Linux Devices](https://fosdem.org/2022/schedule/event/rauc_ab_updates/)

<small>Editing by: @MartijnBraam,
Music by: [The Passion HiFi](http://www.thepassionhifi.com)</small>
