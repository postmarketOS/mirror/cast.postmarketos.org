date: 2021-11-18
title: "#11 INTERVIEW: kop316 (of mmsd-tng Fame)"
length: "48:49"
timestamps:
  - "00:35 Who are you?"
  - "02:10 mmsd-tng"
  - "10:40 visual voice mail daemon"
  - "19:20 UIs interested in mmsd"
  - "23:10 Phosh antispam"
  - "29:10 Daily driving?"
  - "31:05 Keyboard story"
  - "36:05 Music"
  - "40:02 Mobian"
  - "43:30 Shout out"
  - "44:00 Contact/contributing"
---

Chris Talbot aka @kop316 plays a huge part in getting MMS support on modern
mobile Linux distributions. We sit down virtually and talk about the journey
he went through to make this possible, besides lots of other topics. At one
point some of us hear for the first time what visual voice mail is all about.

Featuring @craftyguy, @z3ntu, @ollieparanoid, @MartijnBraam, @kop316 (in order
of appearance).

Referenced in this episode:

* [Multimedia Messaging Service Daemon - The Next Generation](https://gitlab.com/kop316/mmsd/)

    * [mmsd!22](https://gitlab.com/kop316/mmsd/-/merge_requests/22):
      @craftyguy replaces gweb/gresolv with libsoup/c-ares
    * @anteater's [mms-stack](https://sr.ht/~anteater/mms-stack/) page that
      @ollieparanoid was thinking of regarding prior research
    * only 11 months ago: @kop316
      [experimenting with fuzzy7k's purple-mm-sms plugin](https://source.puri.sm/Librem5/chatty/-/issues/30#note_132467)
    * [mmsd!52](https://gitlab.com/kop316/mmsd/-/merge_requests/52):
      @jpsamaroo adding mmsctl, which is being used by Sxmo
    * mmsd support added to Phosh in
      [chatty!768](https://source.puri.sm/Librem5/chatty/-/merge_requests/768),
      one of multiple MRs. In fact just as we release this podcast episode,
      Chatty 5.0~beta was released as
      [the first version with integrated MMS support!](https://fosstodon.org/@kop316/107292792841668186)
    * [pmaports!2194](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/2194):
      @BobbyTheBuilder's MMS testing ground for postmarketOS

* Fact check: in 2015, MMS auto-delivery was disabled in german telekom
      network to reduce fallout from the android
      [stagefright](https://en.wikipedia.org/wiki/Stagefright_(bug)) security
      hole. Instead of delivering MMS directly, a link was sent via SMS where
      users could open the link in a browser
      ([news article](https://www.heise.de/security/meldung/Telekom-schaltet-MMS-wegen-Android-Luecken-ab-2771732.html)).
      So it wasn't completely unusable, but still quite the impact for people
      who actually used MMS.

* [Visual Voicemail Daemon](https://gitlab.com/kop316/vvmd)

    * Mastodon: [demo video](https://fosstodon.org/@kop316/106755448097557783),
      [screenshot](https://freeradical.zone/@craftyguy/106756979895892493)

* [Phosh antispam](https://gitlab.com/kop316/phosh-antispam)

    * Mastodon: [screenshot](https://fosstodon.org/@kop316/107140709926246878)
    * Meanwhile 2.0 is out and available in lots of distros, including
      [Alpine/pmOS edge](https://gitlab.alpinelinux.org/alpine/aports/-/merge_requests/273739)

* Keyboards

    * [Unicomp](https://en.wikipedia.org/wiki/Unicomp)
    * [Kinesis](https://commons.wikimedia.org/wiki/File:Kinesis-keyboard.jpg),
      with the crazy layout

* Music

    * Chris rockin' with *Bah Ram You* in 2015:
      [video](https://www.youtube.com/watch?v=bLoycREnP_k)

* [Mobian](https://mobian-project.org/)

    * Interview with project lead @a-wai in
      [PEBKAC S01E01](https://pebkac.show/2021/10/01/pebkac-s01e01-mobian-project-interview/)

* How to reach you / contribute?

    * Matrix: `#opensourcemms:matrix.org`
    * IRC: `#opensourcemms` on `OFTC`
    * Open merge requests / issues in the repositories linked above
    * Donate: [kop316](https://liberapay.com/kop316/),
      [EFF](https://supporters.eff.org/donate/),
      [pmOS](https://postmarketos.org/donate),
      [Mobian](https://liberapay.com/mobian/donate)

<small>Editing by: @MartijnBraam,
Music by: [The Passion HiFi](http://www.thepassionhifi.com)</small>
