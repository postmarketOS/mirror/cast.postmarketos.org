date: 2022-10-10
title: "#23 PVR SGX540, Kupfer, UBports installer, SoCs, Phone Testing II"
length: "38:17"
timestamps:
  - "00:39 Welcome"
  - "00:52 PowerVR SGX540"
  - "06:11 Kupfer"
  - "09:49 Pablo's fix"
  - "10:44 Pipewire + HFP"
  - "12:17 UBports installer"
  - "16:21 GNOME Shell on Mobile"
  - "18:24 pmOS SoCs"
  - "24:04 Conferences"
  - "27:29 Automated phone testing II"
  - "35:20 How to contribute"
---
In this episode we talk about the difficulties of automated phone testing.
Again. Because Martijn has been continuously working on it. But that's just
one of the segments, we cover quite a lot of different topics this time
actually.

Featuring @calebccff, @ollieparanoid, @MartijnBraam (in order of
appearance).

Referenced in this episode:

* [sgx540-reversing](https://codeberg.org/Garnet/sgx540-reversing)
    * [blog post](https://garnet.codeberg.page/posts/gpu-reversing/)

* Kupfer
    * [Homepage](https://kupfer.gitlab.io/)
    * [kupferbootstrap](https://gitlab.com/kupfer/kupferbootstrap)
    * [pkgbuilds](https://gitlab.com/kupfer/packages/pkgbuilds)
    * [Danctnix](https://github.com/dreemurrs-embedded/Pine64-Arch)
    * Note from Prawn (project lead of Kupfer): pinephones are not on kupfer yet

* Pablo's GTK fix for GTK4 apps crashing on start up
    * Made it into GTK 4.8.0, which means we were able to drop temp/gtk4.0 again \o/
    * [gtk!4687](https://gitlab.gnome.org/GNOME/gtk/-/merge_requests/4687):
      glcontext: Respect ES API when getting gl version from shared context
    * [gtk#4764](https://gitlab.gnome.org/GNOME/gtk/-/issues/4763)

* [pipewire!1379](https://gitlab.freedesktop.org/pipewire/pipewire/-/merge_requests/1379):
  Add a ModemManager support to native HFP backend

* UBports installer
    * [git repository with good README.md](https://github.com/ubports/ubports-installer)
    * [!2511](https://github.com/ubports/ubports-installer/pull/2511): plugins: initial postmarketOS support

* GNOME Shell on Mobile
    * [GNOME Shell on mobile: An update](https://blogs.gnome.org/shell-dev/2022/09/09/gnome-shell-on-mobile-an-update/)
    * [pma!3404](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/3404): add the gnome-mobile UI
    * [pmOS wiki page](https://wiki.postmarketos.org/wiki/Gnome_Shell_for_Mobile)

* SoCs
    * [Mainlining#Supported_SoCs](https://wiki.postmarketos.org/wiki/Mainlining#Supported_SoCs)
    * [SoC Communities](https://wiki.postmarketos.org/wiki/SoC_Communities)
    * Recently added: [Qualcomm Snapdragon 720G (SM7125)](https://wiki.postmarketos.org/wiki/Qualcomm_Snapdragon_720G_(SM7125)) with samsung-a52q in [pma!3379](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/3379)

* Conferences
    * [KDE Akademy](https://akademy.kde.org/)
    * [FOSDEM 2023](https://fosdem.org/2023/),
      [announcement](https://fosdem.org/2023/news/2022-09-14-fosdem-2023-dates/):
      "While there are still a lot of open questions, we are aiming for a
       physical FOSDEM 2023. It's both exciting and scary."

* Automated Phone Testing: [pt.2](https://blog.brixit.nl/automated-phone-testing-pt-2/)
  and [pt.3](https://blog.brixit.nl/automated-phone-testing-pt-3/)

* How to contribute
    * Caleb: [How you can help the Linux Mobile ecosystem](https://connolly.tech/posts/2022_09_16-how-to-help-linux-mobile/)
    * pmOS wiki: [Contributing](https://wiki.postmarketos.org/wiki/Contributing)


<small>Editing by: @ollieparanoid,
Music by: [The Passion HiFi](http://www.thepassionhifi.com),
@ollieparanoid</small>
