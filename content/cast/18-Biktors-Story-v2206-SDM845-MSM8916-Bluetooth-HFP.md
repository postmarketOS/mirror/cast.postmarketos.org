date: 2022-05-29
title: "#18 Biktor's Story, v22.06, SDM845, MSM8916, Bluetooth HFP"
length: "46:40"
timestamps:
  - "01:21 Biktor's Story"
  - "09:53 Development Process Reflections"
  - "16:36 Biktor's FW using LK2nd"
  - "20:05 v22.06: Release Upgrade Script"
  - "23:31 v22.06: New Devices"
  - "24:28 SDM845 Feedbackd"
  - "26:07 SDM845 Audio"
  - "30:38 postmarketOS-tweaks + Sxmo"
  - "32:40 Bluetooth HFP"
  - "40:50 MSM8916 News"
  - "42:07 Android Dynamic Partitions"

---
What's Biktor's background and motivation for doing all that work with his
alternative EG25-G modem firmware? Will we finally be able to upgrade from one
postmarketOS release to the next one with upcoming v22.06? What's it like to
play around in the SDM845 audio maze? You guessed it, we'll talk about all that
and a whole lot more. Put on some headphones and enjoy!

Featuring @ollieparanoid, @calebccff, @MartijnBraam, @dylanvanassche,
@PureTryOut (in order of appearance).

Referenced in this episode:

* Biktor's Story
    * [Buy Biktor a coffee](https://ko-fi.com/biktorgj)
    * [HTC S710](https://en.wikipedia.org/wiki/HTC_S710) with a
      [Linux port](https://web.archive.org/web/20080327113550/http://vonix.sourceforge.net/wi/index.php/Main_Page)
    * [pinephone_modem_sdk](https://github.com/Biktorgj/pinephone_modem_sdk):
      main repository with a good README, issue tracker etc.
    * [openqti](https://github.com/Biktorgj/meta-qcom/tree/kirkstone/recipes-modem/openqti)
    * Experience from ollieparanoid, as of writing: with Biktor's firmware +
      [adjusting udev rule](https://postmarketos.org/edge/2022/05/23/biktor-modem-fw-disappears/),
      the modem runs much better than with the stock firmware. It's still
      not perfectly reliable for me, lately it crashed after >3 days of working
      fine. But with the open firmware we can track and fix the remaining bugs!
    * [Upgrading to Biktor's firmware with fwupd in postmarketOS](https://wiki.postmarketos.org/wiki/Fwupd)
    * [Release 0.6.5](https://github.com/Biktorgj/pinephone_modem_sdk/releases/tag/0.6.5)
      uses LK2nd

* Upcoming v22.06 Release
    * [postmarketos-release-upgrade](https://gitlab.com/postmarketOS/postmarketos-release-upgrade/)
      (CI fix: [pmb!2185](https://gitlab.com/postmarketOS/pmbootstrap/-/merge_requests/2185))
    * New devices: [Samsung Galaxy S III](https://wiki.postmarketos.org/wiki/Samsung_Galaxy_S_III_(samsung-m0)),
      [SHIFT6mq](https://wiki.postmarketos.org/wiki/SHIFT_SHIFT6mq_(shift-axolotl))

* postmarketOS-tweaks: SXMO X11 support
    * [!15](https://gitlab.com/postmarketOS/postmarketos-tweaks/-/merge_requests/15)
    * [Video](https://www.youtube.com/watch?v=a1kgHRbSFzw)

* Bluetooth
    * [pma!3080](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/3080):
      temp/pulseaudio: fork for Bluetooth HFP/HSP support,
      with a video and lots of details, testers wanted

* MSM8916:
    * [pma!3014](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/3014):
      community/linux-postmarketos-qcom-msm8916: upgrade to 5.17 

* Android Dynamic Partitions:
    * [postmarketOS wiki](https://wiki.postmarketos.org/wiki/Android_dynamic_partitions)

<small>Editing by: @MartijnBraam, @ollieparanoid,
Music by: [The Passion HiFi](http://www.thepassionhifi.com)</small>
