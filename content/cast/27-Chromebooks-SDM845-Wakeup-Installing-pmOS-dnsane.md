date: 2023-01-29
title: "#27 Chromebooks, SDM845 Wakeup for Calls, Installing pmOS, dnsane"
length: "48:09"
timestamps:
- "00:38 New Trusted Contributor"
- "13:19 FOSDEM'23"
- "17:10 SourceHut"
- "21:05 SDM845 Wakeup"
- "25:37 Hackerboards"
- "32:25 Installing postmarketOS"
- "39:51 dnsane"
- "43:42 cool things"
---
Chromebooks! We talked about them in the past, but this time we have our newest
Trusted Contributor Anton aka @jenneron on to tell us more about the ~40 ports
we now have in postmarketOS. Besides that we get excited about FOSDEM and talk
about more cool things cool people did.

Featuring @calebccff, @ollieparanoid, @MartijnBraam, @jenneron (in order of
appearance).

Referenced in this episode:

* New trusted contributor: @jenneron
    * [Team members](https://wiki.postmarketos.org/wiki/Team_members)
    * [Chromebooks](https://wiki.postmarketos.org/wiki/Category:ChromeOS)
    * [Microsoft Surface RT](https://wiki.postmarketos.org/wiki/Microsoft_Surface_RT_(microsoft-surface-rt))

* FOSDEM '23
    * [FOSS on Mobile Devices devroom](https://fosdem.org/2023/schedule/track/foss_on_mobile_devices/)
    * [Linux on Mobile stand](https://fosdem.org/2023/stands/)

* SourceHut
    * [postmarketos#49](https://gitlab.com/postmarketOS/postmarketos/-/issues/49):
      Considering move to SourceHut (2023-01-29 update in top post)
    * [Wiki: Patch review workflow on SourceHut](https://wiki.postmarketos.org/wiki/Patch_review_workflow_on_SourceHut)
      explaining the b4 based workflow

* SDM845 Wakeup for Calls
    * [Caleb's post](https://fosstodon.org/@calebccff/109701115951552004)

* Hackerboards
    * [hackerboards.com](https://hackerboards.com/)
    * [Martijn's blog: Finding an SBC](https://blog.brixit.nl/finding-an-sbc/)

* Rewritten wiki pages
    * [Installation](https://wiki.postmarketos.org/wiki/Installation)
    * [pmbootstrap](https://wiki.postmarketos.org/wiki/Pmbootstrap)

* dnsane
    * [dnsane.git](https://git.sr.ht/~craftyguy/dnsane)
    * In the time between recording and releasing this episode, @fluvf pointed
      out in
      [pmaports!3805](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/3805)
      that dnsmasq could be used as well, and we'll probably do that to solve
      the problem.

* AppStream related
    * [pmaports!3757](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/3757)
      [RFC] temp/alpine-appstream-downloader: new aport
    * [pmaports#1384](https://gitlab.com/postmarketOS/pmaports/-/issues/1384):
      Software Apps (GNOME Software, Plasma Discover) issues and improvements

* PinePhone Modem SDK
    * [Release 0.7.2](https://github.com/the-modem-distro/pinephone_modem_sdk/releases/tag/0.7.2):
      Tell me lies, tell me sweet little lies

<small>Editing by: @MartijnBraam,
Music by: [The Passion HiFi](http://www.thepassionhifi.com)</small>
