date: 2024-03-28
title: "#39 INTERVIEW: magdesign (of Cycling the World Fame)"
length: "1:17:03"
timestamps:
  - "00:00 first of all"
  - "02:19 why"
  - "08:03 food"
  - "10:10 UIs"
  - "11:52 coding on the phone"
  - "14:47 power management"
  - "17:56 ferry tricks"
  - "19:10 OCR"
  - "21:26 censorship and workarounds"
  - "32:18 OP6 display link"
  - "34:15 systemd"
  - "37:12 the trip"
  - "49:40 hitchhiking"
  - "53:15 building from scratch"
  - "53:47 typical day"
  - "56:24 carrying stuff around"
  - "58:40 replacement parts"
  - "60:14 future"
  - "63:00 what Linux Mobile can do"
  - "66:00 OP6 camera"
  - "69:45 shout out"
---
This time postmarketOS user @magdesign shares his amazing and inspiring story
of traveling around the world for the past 1.5 years... on bike! Featuring
@magdesign, @ollieparanoid, @calebccff, @craftyguy (in order of appearance).

Marc and Fiona accept donations at
[brandisbrandisbrand.com](https://brandisbrandisbrand.com/).

Referenced in this episode:

* [User:Magdesign](https://wiki.postmarketos.org/wiki/User:Magdesign) and
  [Sxmo/Tips and Tricks](https://wiki.postmarketos.org/wiki/Sxmo/Tips_and_Tricks):
  impressive wiki pages with lots of notes and tips!
* [Fiona's photos at pixelfed](https://pixelfed.social/cyclingtogether/)
* [PocketVJ: "@cas 😂 then i can bring all i own…"](https://fosstodon.org/@pocketvj/112112699381372413)
* [PocketVJ: "finally an offline translator for #linuxmobile…"](https://fosstodon.org/@pocketvj/112032032626110762)
* [PocketVJ: "Thanks to @frd @kdenlive matrix channel, i created a #linuxmobile friendly #kdenlive layout…"](https://fosstodon.org/@pocketvj/111965427273970958)
* [PocketVJ: "finally installed an offline chatbot on #linuxmobile…"](https://fosstodon.org/@pocketvj/112109814220591652)
* [GitLab blocked in Iran, but GitHub works?!](https://fosstodon.org/@ikt/111975000538467167)
* {{issue|2619|pmaports}}: VPN not routing through GSM
* {{issue|2620|pmaports}}: Hotspot not available for modern Android devices
* [micro text editor](https://micro-editor.github.io/)


<small>Editing by: @ollieparanoid,
Music by: [The Passion HiFi](http://www.thepassionhifi.com)</small>
