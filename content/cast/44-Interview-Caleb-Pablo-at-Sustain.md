date: 2024-11-24
title: "#44 INTERVIEW: Caleb and Pablo (of postmarketOS Fame) at Sustain"
length: "47:43"
timestamps:
  - "00:00 pre-intro"
  - "00:27 intro"
  - "02:33 roll the tape"
  - "39:33 we are back"
---

Caleb and Pablo had a great time as guests of
[Sustain Episode 255](https://podcast.sustainoss.org/255)! We talk about the
experience and play the original episode. Featuring @ollieparanoid,
@pabloyoyoista, @caleb, @richlitt (in order of appearance). Find the original
show notes below (with adjusted timestamps).

<small>Credits for pmOS podcast parts &mdash; Editing: @ollieparanoid,
Music: [The Passion HiFi](http://www.thepassionhifi.com)</small>

<!-- timestamps below adjusted by 2:33 -->

### Original Show Notes

#### Guests

Caleb Connolly | Pablo Correa Gómez

#### Panelist

Richard Littauer

#### Show Notes

In this episode of Sustain, host Richard Littauer is joined by guests Pablo Correa Gómez and Caleb Connolly to explore the development and sustainability of postmarketOS, an open-source Linux distribution designed to extend the life of mobile devices. The team dives into the project's mission, governance, and the community-driven nature of its work. They discuss the challenges related to funding, primarily through grants and Open Collective donations, and the significance of upstreaming Linux kernel support to collaborate with other communities like Alpine Linux. The conversation also highlights the growth of the postmarketOS community, encouraging contributions from both technical and non-technical supporters, and the importance of comprehensive documentation. Additionally, issues of privacy, telemetry, and user support are examined, alongside the steps towards making postmarketOS more professional and economically sustainable. Press download now to hear more!

\[00:04:03\] Pablo explains postmarketOS and its mission to empower people to have full control over their devices and promote sustainability.

\[00:04:45\] Caleb talks about the governance of postmarketOS that started with a few contributors working on a package repository on top of Alpine Linux and overtime more maintainers were added.

\[00:06:32\] There’s a discussion on the structure of the team, how the community around hardware components forms sub-communities bases on common SOCs, and the focus on improving tooling and the ecosystem rather than building a product for end users.

\[00:09:02\] Richard discusses the massive, refurbished phone market and asks about how postmarketOS fits into this ecosystem. Caleb shares their experience working on the OnePlus 6 phone and explains the technical process of making the device work on upstream Linux and the challenges of hardware enablement.

\[00:12:38\] Pablo explains that the project is largely funded by volunteer work and Caleb describes the challenges in deciding which devices to prioritize for hardware enablement and how all hardware work so far has been done by volunteers.

\[00:16:42\] On the importance of upstreaming, Pablo explains that postmarketOS works hard to contribute back to the Linux ecosystem rather that maintaining device-specific patches and postmarketOS is downstream to Alpine Linux but contributes much of its work upstream to maintain sustainability.

\[00:22:42\] Richard asks about how the project builds shared context and onboards new developers and Pablo and Caleb explain how the project relies on its wiki page to provide extensive documentation and how the pmbootstrap tool makes it easier for new contributors to get started with porting new devices to postmarketOS.

\[00:27:34\] Richard asks about telemetry and how the team tracks their impact.

\[00:28:12\] Pablo talks about how they receive community feedback through events like FOSDEM and have seen an increase in donations, social media engagement, and community members.

\[00:31:22\] Caleb reflects on the pros and cons of collecting telemetry, which could help guide development but may also create unwanted challenges by focusing too heavily on specific devices.

\[00:34:03\] What are Pablo and Caleb most excited about for the next year? Pablo is excited about professionalizing the project, starting to pay contributors, and scaling the project’s growth sustainably, and Caleb jokes about looking forward to the “pre-market OS.”

#### Quotes

\[00:14:33\] “We are trying to grow organically, bit by bit, and be able to pay people to do core things where volunteer work doesn’t reach.”

\[00:17:06\] “In the environment we live in, where you have X amount of code per update, it is totally unsustainable.”

\[00:18:41\] “As a distro, we predominately put together the pieces that other people give us.”

\[00:21:36\] “Downstream patches allow to experiment, but long term are a burden. That’s the same for every project.”

\[00:21:55\] “The sustainability goes beyond reducing waste and also goes into the social ecosystem and how we maintain projects.”

\[00:33:06\] “We know we are not ready for end users, but we need to build the structure and economic support.”

#### Spotlight

*   \[00:35:05\] Richard’s spotlight is DOSBox.
*   \[00:35:36\] Pablo’s spotlight is FOSDEM and the FOSDEM team.
*   \[00:36:20\] Caleb’s spotlight is processing.org.

#### Links

*   [SustainOSS](https://sustainoss.org/)
*   [podcast@sustainoss.org](mailto:podcast@sustainoss.org)
*   [richard@sustainoss.org](mailto:richard@sustainoss.org)
*   [SustainOSS Discourse](https://discourse.sustainoss.org/)
*   [SustainOSS Mastodon](https://mastodon.social/tags/sustainoss)
*   [Open Collective-SustainOSS (Contribute)](https://opencollective.com/sustainoss)
*   [Richard Littauer Socials](https://www.burntfen.com/2023-05-30/socials)
*   [Caleb Connolly Website](https://connolly.tech/)
*   [Caleb Connolly-treehouse](https://social.treehouse.systems/@cas)
*   [Pablo Correa Gómez Website](https://postmarketos.org/core-contributors/#pablo-correa-gomez-pabloyoyoista)
*   [Pablo Correa Gómez LinkedIn](https://www.linkedin.com/in/pablo-correa-gomez/)
*   [postmarketOS](https://postmarketos.org/)
*   [postmarketOS (Open Collective Contribute)](https://opencollective.com/postmarketos)
*   [Gnome Shell & Mutter](https://blogs.gnome.org/shell-dev/2022/09/09/gnome-shell-on-mobile-an-update/)
*   [postmarketOS Devices](https://wiki.postmarketos.org/wiki/Devices)
*   [Sustain Podcast-Episode 195: FOSSY 2023 with Denver Gingerich](https://podcast.sustainoss.org/195)
*   [Software Freedom Conservancy](https://sfconservancy.org/)
*   [FOSSY 2025:July 31-August 1](https://2025.fossy.us/)
*   [linaro](https://www.linaro.org/)
*   [postmarketOS Wiki](https://wiki.postmarketos.org/wiki/Devices)
*   [pmbootstrap](https://wiki.postmarketos.org/wiki/Pmbootstrap)
*   [compost.party](https://compost.party/)
*   [pmbootstrap v3 by Caleb Connolly](https://connolly.tech/posts/2024_06_15-pmbootstrap-v3/)
*   [DOSBox](https://www.dosbox.com/)
*   [FOSDEM 2025](https://fosdem.org/2025/)
*   [Processing](https://processing.org/)

#### Credits

*   Produced by [Richard Littauer](https://www.burntfen.com/)
*   Edited by Paul M. Bahr at [Peachtree Sound](https://www.peachtreesound.com/)
*   Show notes by DeAnn Bahr [Peachtree Sound](https://www.peachtreesound.com/)
