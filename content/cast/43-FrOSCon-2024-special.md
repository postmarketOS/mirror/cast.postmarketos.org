date: 2024-09-11
title: "#43 FrOSCon 2024 Special"
length: "23:10"
timestamps:
  - "00:00 shout out"
  - "01:10 favorite talks"
  - "07:06 hacking at the social event"
  - "10:57 quiz time!!"
  - "16:04 porting pixel 3a to mobian"
  - "19:32 wrapping up"

---
As a first, we recorded with a live audience!

Featuring @Adrianyyy, @ollieparanoid, @1peter10, @devrtz, @calebccff,
@Fellintr, @agx, @kuleszdl, @z3ntu, @erebion (in order of appearance).

Social event hacking & soldering:

* [Adrian: "@cas and @elly hacking on the …" - chaos.social](https://chaos.social/@adrianyyy/112979408526029670)
* [Adrian: "Xiaomi Mi Smart Clock (x04g) b…" - chaos.social](https://chaos.social/@adrianyyy/112979428324257377)
* [Luca Weiss: "👀 [ 0.000000] Booting Lin…" - Fosstodon](https://fosstodon.org/@z3ntu/112979757131733926)
* [Xiaomi Mi Smart Clock (xiaomi-x04g) - postmarketOS Wiki](https://wiki.postmarketos.org/wiki/Xiaomi_Mi_Smart_Clock_(xiaomi-x04g))
* [Felix Urbasik: "I had an absolute blast at the…" - Fell's Mastodon Instance](https://ma.fellr.net/@fell/112980119564985760)

Talks:

* [Luca Weiss: "My talk "eSIM management on Qu…" - Fosstodon](https://fosstodon.org/@z3ntu/112996309756604194)
&mdash; this talk was recorded!
* [LINux on MOBile: "I did a talk at #FrOSCon #Linu…" - Fosstodon](https://fosstodon.org/@linmob/112978186214660132)
  &mdash; @1peter10 talked about keeping [linmob.net](https://linmob.net)
  going for four years (!), an absolutely amazing website that allows to
  keep up with all things linux mobile through nice weekly posts, as well
  as his incredibly useful distro-independent app directory at 
  [linuxsmartphoneapps.org](https://linuxphoneapps.org/)!
* [Guido Günther: "Yesterday I had the chance to give a short #phosh…" - Librem Social](https://social.librem.one/@agx/112984280183357026)
&mdash; not mentioned in the episode but this was also a great talk!
* [devrtz :debian:: "Caleb is speaking about UEFI b…" - Fosstodon](https://fosstodon.org/@devrtz/112977277957681346)
* [waitaha: Recovery environment for Android devices with a close-to-mainline kernel](https://github.com/waitaha/waitaha)
* [main/postmarketos-initramfs: add support for multi-device btrfs (!5508) · Merge requests · postmarketOS / pmaports · GitLab](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/5508)
* [Full schedule](https://programm.froscon.org/2024/)
  &mdash; talks in the FOSS on Mobile devroom were not recorded, but this
  page has further links to slides etc.

Pixel 3a port to Mobian:

* [Qualcomm Snapdragon 670 Mainline / linux · GitLab](https://gitlab.com/sdm670-mainline/linux) by @flamingradian
* [pan: "Playing with the pixel 3a's camera :DD, although not ideal, they look very unique in this stage!"](https://seafoam.space/notice/AkpuMuLtkkk26ktxsO)
* [Got Mobian (almost) ported to Pixel 3a / 3a XL - SDF Chatter](https://lemmy.sdf.org/post/12809764)
* [Follow-up: Mobian on the Pixel 3a / 3a XL (getting close) - SDF Chatter](https://lemmy.sdf.org/post/19600671)

Apps mentioned:

* [devrtz :debian:: "Chatty 0.8.5 has released 🚀 …" - Fosstodon](https://fosstodon.org/@devrtz/113005431112076725)
* [Railway](https://mobile.schmidhuberj.de/railway)
* [Satellites](https://codeberg.org/tpikonen/satellite/)

Other related links:

* [FrOSCon website](https://froscon.org/)
* [Colors of Noise: A short look back at the FOSS on Mobile Devroom at FrOSCon 2024](https://honk.sigxcpu.org/con/A_short_look_back_at_the_FOSS_on_Mobile_Devroom_at_FrOSCon_2024.html)

<small>Editing by: @ollieparanoid,
Music by: [The Passion HiFi](http://www.thepassionhifi.com)</small>
