date: 2021-12-16
title: "#12 v21.12, TTYescape, Mainline OP6/SHIFT6mq/Fairphones, pmbootstrap"
length: "37:25"
timestamps:
  - "00:43 Trusted Contributors"
  - "01:29 Plasma Mobile Gear 21.12"
  - "03:40 More GNOME 41, less forks"
  - "05:29 TTYescape 0.2"
  - "08:49 mobile-config-firefox 3.0.0"
  - "13:28 New devices in community"
  - "14:50 Mainline sdm845"
  - "17:42 SHIFT6mq"
  - "19:24 Mainline Fairphones"
  - "23:27 pmbootstrap news"
  - "33:05 Podcast picks"
  - "35:20 Outro"
---
It has been one year since we launched the podcast, and this time it's mostly
about the upcoming v21.12 release. Among other topics, we have a follow up on 
TTYescape, which Caleb introduced in their interview
in [#8](/episode/08-Interview-Caleb-Connolly-SDM-845-Mainlining/). So grab a
pair of headphones or blast it through your speakers if that's your thing, and
enjoy.

Featuring @ollieparanoid, @PureTryOut, @calebccff, @z3ntu, @Newbyte (in order of
appearance).

Referenced in this episode:

* [Plasma Mobile Gear 21.12](https://plasma-mobile.org/2021/12/07/plasma-mobile-gear-21-12/)
  ([aports!28208](https://gitlab.alpinelinux.org/alpine/aports/-/merge_requests/28208))

* Most GNOME apps upgraded to
  [GNOME 41](https://help.gnome.org/misc/release-notes/41.0/) with
  cherry-picked patches instead of forks
  ([pma#1247](https://gitlab.com/postmarketOS/pmaports/-/issues/1247),
   [aports!26028](https://gitlab.alpinelinux.org/alpine/aports/-/merge_requests/26028))

* [TTYescape](https://wiki.postmarketos.org/wiki/TTYescape) 0.2
    * [Mastodon post with short demo](https://fosstodon.org/@calebccff/107454384759558261)
    * [HKDM - HotKey Daemon (for) Mobile](https://gitlab.com/postmarketOS/hkdm)
    * [buffyboard](https://gitlab.com/cherrypicker/buffyboard)
    * [pma!2713](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/2713)

* [mobile-config-firefox](https://gitlab.com/postmarketOS/mobile-config-firefox)
    3.0.0
    * [Thread with screenshots](https://fosstodon.org/@ollieparanoid/107394745970284867)
    * Mentioned kiosk bug has been fixed
      ([#29](https://gitlab.com/postmarketOS/mobile-config-firefox/-/issues/29))

* New devices in the
  [community category](https://wiki.postmarketos.org/wiki/Device_categorization)
  for the upcoming v21.12 release
    * [Lenovo A6000](https://wiki.postmarketos.org/wiki/Lenovo_A6000_(lenovo-a6000))
    * [Lenovo A6010](https://wiki.postmarketos.org/wiki/Lenovo_A6010_(lenovo-a6010))
    * [PINE64 Pinebook Pro](https://wiki.postmarketos.org/wiki/PINE64_Pinebook_Pro_(pine64-pinebookpro))
    * [PINE64 RockPro64](https://wiki.postmarketos.org/wiki/PINE64_RockPro64_(pine64-rockpro64))
    * [Samsung Galaxy Tab A 8.0 (2015)](https://wiki.postmarketos.org/wiki/Samsung_Galaxy_Tab_A_8.0_2015_(samsung-gt58))
    * [Samsung Galaxy Tab A 9.7 (2015)](https://wiki.postmarketos.org/wiki/Samsung_Galaxy_Tab_A_9.7_2015_(samsung-gt510))
    <small>device page has [a cute photo](https://wiki.postmarketos.org/wiki/File:Gt510-interior.jpg) :D</small>
    * [Xiaomi Poco F1](https://wiki.postmarketos.org/wiki/Xiaomi_Poco_F1_(xiaomi-beryllium))


* [Mainline SDM845](https://wiki.postmarketos.org/wiki/Qualcomm_Snapdragon_845_(SDM845))
    * Upgrade to 5.16-rc4 ([pma!2690](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/2690))

    * [SHIFT6mq](https://wiki.postmarketos.org/wiki/SHIFT_SHIFT6mq_(shift-axolotl))
        * shift-axolotl: new device ([pma!2703](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/2703))
        * shift-axolotl: switch to mainline ([pma!2738](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/2738))
        * [unboxing photos](https://fosstodon.org/@calebccff/107366550596216165)

* [Mainline all the Fairphones!](https://lists.sr.ht/~postmarketos/upstreaming?search=fairphone)

* [pmbootstrap](https://gitlab.com/postmarketOS/pmbootstrap)
    * Pretty much all of the mentioned improvements made it into the
      [1.40.0](https://gitlab.com/postmarketOS/pmbootstrap/-/tags/1.40.0)
      release, or one of the
      [previous releases](https://gitlab.com/postmarketOS/pmbootstrap/-/tags)
    * `pmbootstrap -y init` is still WIP
      ([pma!2139](https://gitlab.com/postmarketOS/pmbootstrap/-/merge_requests/2139))

* Podcast picks
    * [Tehno klistir: Posttržno komunikalo](https://radiostudent.si/druzba/tehno-klistir/posttrzno-komunikalo)
    * [Cyberdeck Users Weekly: Linux on mobile with linmob](https://anchor.fm/futurepaul/episodes/Linux-on-mobile-with-Linmob-e1a0o3k)

<small>Editing by: @MartijnBraam,
Music by: [The Passion HiFi](http://www.thepassionhifi.com)</small>
