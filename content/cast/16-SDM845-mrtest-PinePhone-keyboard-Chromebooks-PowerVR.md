date: 2022-03-27
title: "#16 SDM845, mrtest, PinePhone keyboard, Chromebooks, PowerVR"
length: "32:38"
timestamps:
  - "00:25 New core team members!"
  - "01:33 SDM845"
  - "09:10 mrtest"
  - "22:00 PinePhone keyboard"
  - "24:10 Chromebooks"
  - "27:20 PowerVR"

---
"Allow yourself to spend time on things today that create more time
tomorrow." - with that in mind, the new mrtest tool was created. In fact, it
was just finished up and released as 1.0.0 along with this episode. Even the
aports.git support got implemented! You probably think "what the heck is
mrtest" now, and don't worry, that exact question does get answered in this
episode along with other great news. Grab your headphones, let's go!

Featuring @craftyguy, @ollieparanoid, @calebccff, @dylanvanassche,
@MartijnBraam (in order of appearance).

Referenced in this episode:

* SDM845 mainline news
    * [awesome kernel upgrades](https://gitlab.com/postmarketOS/pmaports/-/merge_requests?scope=all&state=merged&author_username=amartinz&search=sdm845)
      done by @amartinz from SHIFT
    * enable call audio on sdm845 devices
     ([pma!2982](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/2982))

* mrtest
    * [Wiki article](https://wiki.postmarketos.org/wiki/Mrtest) with install
      and usage instructions + examples

* Chromebooks
    * Support ChromeOS partition table and kernel partition
      ([pmb!2163](https://gitlab.com/postmarketOS/pmbootstrap/-/merge_requests/2163))
    * wiki: [ChromeOS devices](https://wiki.postmarketos.org/wiki/ChromeOS_devices)
    * Thanks to Jenneron!

* PinePhone keyboard:
    * Thanks, megi!

* PowerVR
    * Martijn's article on tuxphones:
      [What the new PowerVR driver means for mobile](https://tuxphones.com/what-does-the-new-powervr-driver-mean-for-mobile/)

<small>Editing by: @MartijnBraam,
Music by: [The Passion HiFi](http://www.thepassionhifi.com)</small>
