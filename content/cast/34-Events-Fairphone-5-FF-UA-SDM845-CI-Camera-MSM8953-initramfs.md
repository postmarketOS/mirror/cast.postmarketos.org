date: 2023-09-28
title: "#34 Events, Fairphone 5, FF UA, SDM845 CI & Camera, MSM8953, Initramfs"
length: "46:39"
timestamps:
  - "00:09 Welcome"
  - "00:40 FSiC 2023"
  - "01:40 Akademy 2023"
  - "03:33 QT6 Plasma Mobile"
  - "05:40 GUADEC 2023"
  - "08:51 FrOSCon"
  - "10:07 BananUI"
  - "12:16 Crossdirect + Rust"
  - "13:10 Tow-Boot"
  - "15:53 Fairphone 5"
  - "20:48 mobile-config-firefox"
  - "28:58 SDM845"
  - "34:35 mtkclient"
  - "35:40 MSM8953"
  - "37:14 Initramfs"
  - "42:52 New merch"
---
Luca did it again! On the day the Fairphone 5 was released, he added it to
postmarketOS. How exactly that is possible and a whole lot more about recent
developments and Linux Mobile related events we followed in this episode.

Featuring @calebccff, @ollieparanoid, @z3ntu, @pabloyoyoista (in order of
appearance).

Referenced in this episode:

* [Free Silicon Conference 2023](https://wiki.f-si.org/index.php/FSiC2023)
    * Luca's talk:
      [Open Source for Sustainable and Long lasting Phones](https://wiki.f-si.org/index.php?title=Open_Source_for_Sustainable_and_Long_lasting_Phones)
      ([PeerTube](https://peertube.f-si.org/videos/watch/8b581083-0f56-40e5-bcab-a5a8ab8c1b71),
      [YouTube](https://www.youtube.com/watch?v=_tbUGUWOiwU))

* [Akademy 2023](https://akademy.kde.org/2023/)

* [GUADEC 2023](https://events.gnome.org/event/101/)
    * Keynote: [Global collaborative communities to empower cutting-edge technologies](https://www.youtube.com/watch?t=2595&v=qaZuQO0bK-M)
    * Pablo's lightning talk: [Booting GNOME in 150+ phones](https://www.youtube.com/watch?v=qaZuQO0bK-M&t=23430s)

* [FrOSCon 2023](https://froscon.org/en/)
    * devrtz's talk:
      [The year of Linux On Desktop^WMobile](https://media.ccc.de/v/froscon2023-2950-the_year_of_linux_on_desktop_wmobile)
    * LINMOB.net blog post: [Hello, FrOSCon 2023](https://linmob.net/hello-froscon/)
    * Affe Null's slides: [FOSS on Feature Phones](https://programm.froscon.org/2023/system/event_attachments/attachments/000/000/778/original/phones.pdf)

* Crossdirect + Rust improvements:
    * cross/crossdirect: improve rust handling ({{MR|4234|pmaports}})
    * cross/crossdirect: partially use sccache for rust ({{MR|4310|pmaports}})
    * pmbootstrap: sccache for rust, refactor /mnt/pmb ({{patches|43386|pmbootstrap-devel}})

* Tow-Boot
    * [Open call for help and contributions](https://gist.github.com/samueldr/8750e4aabbc23badec7a8798cabd4ed1)
    * [2022.07-006](https://github.com/Tow-Boot/Tow-Boot/releases/tag/release-2022.07-006) released in 2023-07

* Fairphone 5
    * fairphone-fp5: new device ({{MR|4359|pmaports}})
    * Initial support for the Fairphone 5 smartphone ({{patches|44172|upstreaming}})
    * [A sign of DisplayPort over USB-C (external display) working with the #mainline kernel …](https://fosstodon.org/@z3ntu/111132695265417281)
    * [Fairphone 5 source code release](https://forum.fairphone.com/t/fairphone-5-source-code-release/99616)

* mobile-config-firefox
    * fix: add mobile to user-agent ({{MR|35|mobile-config-firefox}})
    * [History of the browser user-agent string](https://webaim.org/blog/user-agent-string-history/)

* SDM845
    * Recent Qualcomm camera development on the mailing lists, likely applies to SDM845 if anyone wants to try it:
      ([1](https://lore.kernel.org/linux-arm-msm/20230830151615.3012325-1-bryan.odonoghue@linaro.org/)),
      ([2](https://lore.kernel.org/linux-arm-msm/20230911131411.196033-1-bryan.odonoghue@linaro.org/))
    * Also new version of IMX519 sensor, the one in the SHIFT6mq and OnePlus 6:
      ([3](https://lore.kernel.org/all/20230908124344.171662-1-umang.jain@ideasonboard.com/))

* [mtkclient](https://github.com/bkerler/mtkclient)
    * pmbootstrap: Add mtkclient as a flasher option ({{patches|42621|pmbootstrap-devel}})

* [MSM8953](https://wiki.postmarketos.org/wiki/Qualcomm_Snapdragon_450/625/626/632_(MSM8953))
    * qcom-msm8953: move devices to community ({{MR|4381|pmaports}})

* Initramfs
    * main/postmarketos-initramfs: simplify and improve the boot process
      ({{MR|4204|pmaports}})
    * oneplus 6 userdata not expanded during install ({{issue|2235|pmaports}})

* [Merch](https://postmarketos.org/merch)
    * [3 new designs](https://fosstodon.org/@ollieparanoid/111105706511584106) for
      our [shirts site](https://postmarketos.teemill.com/)

<small>Editing by: @ollieparanoid,
Music by: [The Passion HiFi](http://www.thepassionhifi.com)</small>
