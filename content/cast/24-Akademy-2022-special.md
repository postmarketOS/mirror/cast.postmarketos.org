date: 2022-10-16
title: "#24 Akademy 2022 Special"
length: "31:10"
timestamps:
  - "00:11 Okay so..."
---
It seems Akademy 2022 had it all. Amazing talks, great people, nice weather, a
tour through Barcelona as well as a nearby supercomputer, climbing in a
mountain and for some, pizza on the beach. Or as Bart puts it, Akademy was kind
of dope to be honest.

Featuring @ollieparanoid, @PureTryOut, @MartijnBraam, @z3ntu (in order of
appearance).

Referenced in this episode:

* [Akademy 2022 Homepage](https://akademy.kde.org/2022) with talk schedule etc.

* Recordings of the talks will probably show up soon at:
  [https://cdn.files.kde.org/akademy/2022](https://cdn.files.kde.org/akademy/2022/)

* Plasma Mobile in 2022: [stream](https://tube.kockatoo.org/w/1Yd26fiAAhmrZnVvGcoC86?start=4h56m41s),
  [talk details](https://cdn.files.kde.org/akademy/2022/)

* Jonathan Esk-Riddel's diary:
  [Akademy 2022 in Barcelona Day 1 Talks](https://jriddell.org/2022/10/02/akademy-2022-in-barcelona-day-1-talks/),
  great blog post (also the follow-ups), featuring a nice photo of Bhushan on
  the big screen

* Nate Graham's blog: [Adventures in Linux and KDE](https://pointieststick.com/)

* Wikipedia: [MareNostrum](https://en.wikipedia.org/wiki/MareNostrum)
  supercomputer

<small>Editing by: @MartijnBraam,
Music by: [The Passion HiFi](http://www.thepassionhifi.com)</small>
