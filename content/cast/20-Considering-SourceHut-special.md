date: 2022-07-25
title: "#20 Considering SourceHut Special"
length: "41:26"
timestamps:
  - "00:00 Intro"
  - "00:54 Current status"
  - "01:32 Why consider moving"
  - "05:11 Considering various platforms"
  - "07:03 Why not selfhost GitLab?"
  - "07:56 Considering SourceHut"
  - "09:58 Patch workflow"
  - "17:42 Roadmap pt. 1"
  - "19:39 Why move pmbootstrap.git first"
  - "20:50 Caleb's experience / productivity"
  - "24:00 Roadmap pt. 2 / team meetings"
  - "25:45 Martijn's take"
  - "27:36 Caleb's take"
  - "30:38 Ollie's take"
  - "33:13 Clayton's take"
  - "35:26 Roadmap pt. 3 / teaching new users"
  - "37:47 What are your thoughts?"
---
Here's something different. We have been considering to move from gitlab.com to
[SourceHut](https://sourcehut.org/) and this episode right here is the
companion to the [blog post](https://postmarketos.org/blog/2022/07/25/considering-sourcehut/)
we just released. We decided to do both so people can get a TL;DR version on
the blog with the most important information and the tentative roadmap. But
also a fourtyone minute conversation where we go deep. There is a lot to talk
about, and this episode is like a glimpse into the many hours of team meetings
we had on this topic before the announcement. Featuring @craftyguy,
@ollieparanoid, @calebccff, @MartijnBraam (in order of appearance).

If you have contributed to postmarketOS in the past: looking forward to your
thoughts on this in
[postmarketos#49](https://gitlab.com/postmarketOS/postmarketos/-/issues/49).


<small>Editing by: @ollieparanoid,
Music by: [The Passion HiFi](http://www.thepassionhifi.com)</small>
