date: 2025-02-10
title: "#45 FOSDEM 2025 Special"
length: "20:02"
timestamps:
  - "00:01 Waffles"
---

As FOSDEM 2025 was coming to a close, we sat down in the cafeteria to talk
about the two amazing days that we just had behind us.

Featuring @Newbyte, @z3ntu, @craftyguy, @PureTryOut, @anjandev, @f-izzo,
@ungeskriptet, @Adrianyyyy, @camelCaseNick, Philipp, Marijn (mostly in order of
appearance).

**Not included in the episode:** a three day post-FOSDEM hackathon, read all about it
[on the blog](https://postmarketos.org/blog/2025/02/10/fosdem-and-hackathon/)!

Video recordings of the talks:

* [FOSS on Mobile Devices devroom](https://fosdem.org/2025/schedule/track/mobile/)
* [LINMOB.net list of talk recommendations](https://linmob.net/fosdem2025/)
* [All talks](https://fosdem.org/2025/schedule/events/)

Related episodes:

* \#38 FOSDEM 2024 special
* \#28 FOSDEM 2023 special
* \#14 FOSDEM 2022 special

<small>Editing by: @ollieparanoid,
Music by: [The Passion HiFi](http://www.thepassionhifi.com)</small>
