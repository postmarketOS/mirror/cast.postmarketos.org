date: 2024-04-30
title: "#40 INTERVIEW: Devin Lin (of Plasma Mobile Fame)"
length: "42:45"
timestamps:
  - "00:00 devices"
  - "01:18 origin story"
  - "03:43 nav buttons"
  - "08:14 app names"
  - "12:52 two launchers"
  - "15:18 focus on tooling"
  - "20:04 nightly repo"
  - "21:44 akademy"
  - "24:38 how to contribute"
  - "30:30 shout out"
  - "33:20 alpine/pmOS maintenance"
  - "37:17 what's next"
  - "38:05 daily driving"
  - "40:23 wrap up"
---

Not long after the recent Plasma 6 megarelease, we are excited to hear from
Plasma Mobile's lead developer! Featuring @espidev, @ollieparanoid,
@PureTryOut, @calebccff (in order of appearance).

Referenced in this episode:

* [Plasma Mobile](https://plasma-mobile.org/)
  &mdash; main website
* [Daily driving Plasma Mobile](https://fam-ribbers.com/blog/2024-05-05-daily-driving-plasma-mobile/)
  &mdash; *Bart published this blog post shortly after the episode was aired,
  he details his experience of using Plasma Mobile and explains how you can
  build any plasma package from a local git tree with
  `pmbootstrap build --src`*
* [Plasma 6 and Beyond!](https://plasma-mobile.org/2024/03/01/plasma-6/)
  &mdash; *"Plasma Mobile joins the Plasma 6 megarelease with a new shell and apps"*
* [plasma-mobile!454](https://invent.kde.org/plasma/plasma-mobile/-/merge_requests/454)
  &mdash; *"taskswitcher: enhance gesture with flick speed tracking and orthogonal movement tracking"* by Luis Büchi
* [Matrix channel](https://matrix.to/#/#plasmamobile:matrix.org)
  &mdash; *"most active with developers"*
* [Forum](https://discuss.kde.org/c/development/plasma-mobile/11)
  &mdash; at KDE Discuss
* [Wiki](https://invent.kde.org/plasma/plasma-mobile/-/wikis/home)
  &mdash; *"konqi wants you to contribute!"*

Related episodes:

* \#24 Akademy 2022 Special

<small>Editing by: @ollieparanoid,
Music by: [The Passion HiFi](http://www.thepassionhifi.com)</small>
