date: 2022-08-07
title: "#21 INTERVIEW: Sebastian Krzyszkowiak (of Phosh,\nLibrem 5 Fame)"
length: "52:45"
timestamps:
  - "00:32 Introduction"
  - "01:00 Phosh History"
  - "03:05 Why wlroots based?"
  - "08:05 GNOME Shell on mobile thoughts?"
  - "15:00 Lockscreen magic"
  - "18:11 Exciting Phoc developments"
  - "23:00 Phosh Gestures"
  - "26:05 pmOS experience"
  - "27:20 N900"
  - "30:42 Purism's role in Linux Mobile"
  - "36:00 scale-to-fit"
  - "39:14 Future/suspend"
  - "49:05 Shout out"
  - "51:09 Guido's talk"
---
Even though time is precious when participating in Game Jams, Sebastian does
not simply generate sound effects like everybody else - instead he records
himself playing them on real instruments. That's what we talked about when we
were done with recording this episode anyway. What he does talk about here is
the Die Hard GNU/Linux User On Phones side of himself, and how he channels it into
developing all these amazing Phoc/Phosh and Librem 5 improvements! Also how he
has used a certain non-GNU libc based mobile Linux distribution, and how he
fulfilled his dreams of shrinking windows.

Featuring @ollieparanoid, @calebccff, @MartijnBraam, @dos (in order of
appearance).

Referenced in this episode:

* git repos: [phoc](https://gitlab.gnome.org/World/Phosh/phoc),
  [phosh](https://gitlab.gnome.org/World/Phosh/phosh),
  [wlroots](https://gitlab.freedesktop.org/wlroots/wlroots/)

* @afontain's Swosh (Sw<del>ay</del> + <del>Ph</del>osh) experiments, with a
  photo:
  [pma!1357](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/1357)

* [Towards GNOME Shell on mobile](https://blogs.gnome.org/shell-dev/2022/05/30/towards-gnome-shell-on-mobile/)

* Nokia N900: [Maemo OG](http://maemo.org/) and
  [Maemo Leste](https://maemo-leste.github.io/)

* pmOS blog:
  [Why supporting the Librem Phone crowdfunding campaign helps postmarketOS (and friends)](https://postmarketos.org/blog/2017/09/24/librem-5/)

* scale-to-fit: [source](https://gitlab.gnome.org/World/Phosh/phoc/-/blob/master/helpers/scale-to-fit),
  [pmOS wiki](https://wiki.postmarketos.org/wiki/Phosh#Screen_Scaling),
  [Purism post](https://wp.puri.sm/posts/easy-librem-5-app-development-scale-the-screen/),
  [@linmob's howto](https://linmob.net/pinephone-setup-scaling-in-phosh/)

* @craftyguy's post about
  [postmarketOS on Librem 5](https://puri.sm/posts/adventures-of-porting-postmarketos-to-the-librem-5/)

* Guido's DebConf22 talk:
  [The current state of Debian on smartphones](https://meetings-archive.debian.net/pub/debian-meetings/2022/DebConf22/debconf22-231-the-current-state-of-debian-on-smartphones.webm)


<small>Editing by: @MartijnBraam,
Music by: [The Passion HiFi](http://www.thepassionhifi.com)</small>
