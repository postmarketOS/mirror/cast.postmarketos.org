date: 2023-05-29
title: "#31 Testing Team, KDE6, SDM845 Sensors, ALIT, PineNote, L10N"
length: "49:17"
timestamps:
  - "00:23 v23.12 relup"
  - "01:10 testing team"
  - "05:48 kde6"
  - "09:12 akademy"
  - "10:07 sdm845 sensors"
  - "20:50 wakeup for calls / opportunistic sleep"
  - "25:41 alit"
  - "33:14 pinenote"
  - "36:37 l10n"
  - "44:32 dnsane update"
  - "47:00 postmarketos-mkinitfs"
---
You rotate your phone and the UI rotates as well. Sounds simple, right? In this
episode Caleb explains us why implementing it for SDM845 requires far more
complexity than one may think - among many other topics.

Featuring @ollieparanoid, @PureTryOut, @calebccff, @pabloyoyoista (in order of
appearance).

Referenced in this episode:

* Testing
    * [Call For Testers](https://postmarketos.org/blog/2023/05/21/call-for-testers/)
    * [Testing Team](https://wiki.postmarketos.org/wiki/Testing_Team)

* KDE
    * [kde-srcbuild](https://invent.kde.org/sdk/kdesrc-build)
    * [Akademy 2023](https://akademy.kde.org/2023/)

* SDM845
    * [Qualcomm Snapdragon Sensor Core support for SDM845 devices](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/4050) (pma!4050)
    * [Wakeups](https://wiki.postmarketos.org/wiki/Qualcomm_Snapdragon_845_(SDM845)#Wakeups) -
      if you would like to look into this: "joining matrix would be a a good
      place to start. That being said, it might be worth noting that folks
      should already have some familiarity with the kernel suspend/resume
      process and with WoWLAN/TCP/push notifications"
    * [Fedora Mobile Remix for OnePlus 6](https://gitlab.com/fedora/sigs/mobility/sdm845/op6)

* Augsburger Linux-Infotag 2023
    * [LINMOB.net - Having a stand at Linux-Infotag 2023](https://linmob.net/lit-2023-stand-experiences/)
      nice blog post with photos
    * [Handout](https://pinephone.de/flyer.pdf)
    * [Can My Phone Run Linux?](https://many.tuxphones.com/)
    * [TT173 Augsburger Linux-Infotag 2023 - TechnikTechnik](https://techniktechnik.de/?podcast=tt173-augsburger-linux-infotag-2023)
    * [Linux-Smartphones - FOCUS ON: Linux - Podcast](https://focusonlinux.podigee.io/50-linux-smartphones)

* PineNote
    * [Initial MR](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/3778) (pma!3778)
    * [Wiki page](https://wiki.postmarketos.org/wiki/PINE64_PineNote_(pine64-pinenote))

* Localization
    * Ping @pabloyoyoista if you would like to get involved
    * [Default file sorting](https://gitlab.com/postmarketOS/pmaports/-/issues/1446) (pma#1446)
    * [Translating postmarketOS](https://wiki.postmarketos.org/wiki/Translating_postmarketOS)

* [pmos-base-ui-networkmanager: set up dnsmasq for filtering lookup](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/3823) (pma!3823)

* [postmarketos-mkinitfs changes](https://gitlab.com/postmarketOS/postmarketos-mkinitfs/-/tags)

<small>Editing by: @ollieparanoid,
Music by: [The Passion HiFi](http://www.thepassionhifi.com), @ollieparanoid
</small>
