date: 2021-10-13
title: "#10 SWMO, Waydroid, XFCE4, MATE, msm-fw-loader, mkinitfs, OctoPrint"
length: "34:52"
timestamps:
  - "00:32 SWMO"
  - "04:29 Waydroid"
  - "09:50 XFCE4/MATE"
  - "12:07 UI wiki page"
  - "14:10 LG G Watch R"
  - "15:30 msm-firmware-loader"
  - "19:39 boot-deploy/mkinitfs"
  - "25:53 OctoPrint"
  - "32:25 10th episode!"
---

Following up on WayDroid since we last talked about it in #8 - still
experimental but now properly packaged for edge. So less hacking around to
give it a try. Besides that &mdash; as always &mdash; we have a fun time with a
whole bunch of other news. For feedback use `#postmarketOSpodcast` on
[Mastodon](https://fosstodon.org/@postmarketOS).


Featuring @craftyguy, @PureTryOut, @z3ntu, @MartijnBraam, @ollieparanoid (in
order of appearance).

Referenced in this episode:

* SWMO
    * [sxmo-tickets#339](https://todo.sr.ht/~mil/sxmo-tickets/339):
      SWMO - Wayland Version of SXMO 
    * [interview with proycon](/episode/05-Interview-with-proycon-from-sxmo/)
      in episode #5

* WayDroid
    * [new homepage](https://waydro.id/) with fancy domain name
    * [postmarketOS wiki page](https://wiki.postmarketos.org/wiki/Waydroid)
    * [CONFIG_PSI performance video](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/2497#note_674911184)
    * sharing DNA with pmbootstrap
        * e.g.
      [here](https://github.com/waydroid/waydroid/blob/80555aba69dd568aa989edea8480487e87d80aac/tools/helpers/run.py)
        * happy that the code could be repurposed! :)

* XFCE4 & MATE
    * [xfce4-phone.git](https://gitlab.com/postmarketOS/xfce4-phone)
    * [pma!2464](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/2464):
      both: onboard keyboard
    * [pma!2506](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/2506):
      XFCE4: further improvements
    * [pma!2525](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/2525):
      MATE: change terminal colors

* All-new [Category:Interface](https://wiki.postmarketos.org/wiki/Category:Interface)
  wiki page

* LG G Watch R mainlining
    * patchset: [initial LG G Watch R support](https://lists.sr.ht/~postmarketos/upstreaming/patches/25119)
    * wiki: 
      [device page](https://wiki.postmarketos.org/wiki/LG_G_Watch_R_(lg-lenok)),
      [mainlining](https://wiki.postmarketos.org/wiki/Mainlining)

* msm-firmware-loader
    * [pma!2431](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/2431):
      add a package to load firmware on msm devices 
    * [msm8916-mainline/lk2nd](https://github.com/msm8916-mainline/lk2nd)
    * somewhat related: Caleb (they/them)'s efforts for a generic sdm845 port in
      [pma!2566](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/2566)

* boot-deploy / postmarketos-mkinitfs >= 1.0.0
    * [edge blog announcement](https://postmarketos.org/edge/2021/09/03/new-mkinitfs/)
    * [boot-deploy](https://gitlab.com/postmarketOS/boot-deploy), discussion
      leading up to it in [pma#1152](https://gitlab.com/postmarketOS/pmaports/-/issues/1152)
    * [pma#1019](https://gitlab.com/postmarketOS/pmaports/-/issues/1019) has
      details why rewriting postmarketos-mkinitfs was needed

* Martijn's OctoPrint setup on the PinePhone
    * [blog post](https://blog.brixit.nl/running-octoprint-on-the-pinephone/)
    * video:
    [peertube](https://spacepub.space/w/xpkUVzdVBvGLka8yfZg2k5),
    [youtube](https://www.youtube.com/watch?v=fUt5tDfD93U)

* 10th episode!
     * [blinky lights are crucial for podcast editing](https://fosstodon.org/@postmarketOS/106960820754824725)
     * \#6 was split up

* PineTalk is back
     * [homepage](https://www.pine64.org/pinetalk/)
     * feeds:
       [opus](https://www.pine64.org/feed/opus/),
       [mp3](https://www.pine64.org/feed/mp3/)

<small>Editing by: @MartijnBraam,
Music by: [The Passion HiFi](http://www.thepassionhifi.com)</small>
