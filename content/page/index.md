title: postmarketOS Podcast
---

You just found the corner of the Internet, where postmarketOS team members
discuss whatever crazy development details we encounter while cooking up our
favorite mobile Linux distribution. Besides news and anecdotes, once in a while
we throw in an interview with an exciting guest who brings us their unique view
on the wider mobile Linux scene.

Subscribe your podcatcher to:
[`https://cast.postmarketos.org/feed.rss`](https://cast.postmarketos.org/feed.rss)

<small>
Apple devices need the legacy MP3 feed:
[`https://cast.postmarketos.org/feed-legacy.rss`](https://cast.postmarketos.org/feed-legacy.rss)
</small>

Find podcast apps for your Linux phone at:
[LinuxPhoneApps.org](https://linuxphoneapps.org/categories/podcast-client/)
