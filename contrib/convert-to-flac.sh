#!/bin/sh -e
# Copyright 2024 Oliver Smith
# SPDX-License-Identifier: AGPL-3.0-or-later
file="$1"
title="$2"
work="$(pmbootstrap -q config work)"
tmp="$work/chroot_native/tmp"

if [ $# != 1 ]; then
	echo "usage: convert-to-flac.sh SOURCE_FILE"
	exit 1
fi

set -x
sudo rm -f "$tmp/out.flac"

if ! [ -d "$tmp" ]; then
	pmbootstrap -q chroot -- true
fi

cp "$1" "$tmp/in"

pmbootstrap -q chroot --add=ffmpeg -- \
	ffmpeg \
		-i "/tmp/in" \
		-ac 1 \
		"/tmp/out.flac"

cp "$tmp/out.flac" "${1%.*}.flac"
