#!/bin/sh -e
# Copyright 2023 Oliver Smith
# SPDX-License-Identifier: AGPL-3.0-or-later
file="$1"
title="$2"
work="$(pmbootstrap -q config work)"
tmp="$work/chroot_native/tmp"

if [ $# != 1 ]; then
	echo "usage: convert-to-opus.sh EPISODE_FILE"
	exit 1
fi

set -x
sudo rm -f "$tmp/out.opus"

if ! [ -d "$tmp" ]; then
	pmbootstrap -q chroot -- true
fi

cp "$1" "$tmp/in"

pmbootstrap -q chroot --add=ffmpeg -- \
	ffmpeg \
		-i "/tmp/in" \
		-ac 1 \
		-b:a 32K \
		"/tmp/out.opus"

cp "$tmp/out.opus" "${1%.*}.opus"
