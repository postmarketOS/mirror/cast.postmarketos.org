#!/bin/sh -e
# Copyright 2023 Oliver Smith
# SPDX-License-Identifier: AGPL-3.0-or-later
file="$1"
title="$2"
work="$(pmbootstrap -q config work)"
tmp="$work/chroot_native/tmp"

if [ $# != 2 ]; then
	echo "usage: add-tags.sh EPISODE_FILE TITLE"
	exit 1
fi

set -x
sudo rm -f "$tmp/out.opus"

if ! [ -d "$tmp" ]; then
	pmbootstrap -q chroot -- true
fi

cp "$1" "$tmp/in.opus"

pmbootstrap -q chroot --add=ffmpeg -- \
	ffmpeg \
		-i "/tmp/in.opus" \
		-acodec copy \
		-metadata:s:a ARTIST="postmarketOS podcast" \
		-metadata:s:a TITLE="$title" \
		"/tmp/out.opus"

cp "$tmp/out.opus" "$1"
